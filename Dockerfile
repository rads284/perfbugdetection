FROM ubuntu
RUN apt-get update 
RUN apt-get install -y clang-6.0 
RUN apt-get install -y python3 
RUN apt-get install -y    git 
RUN apt-get install -y vim
RUN apt-get update
RUN apt-get install -y python3-pip
RUN pip3 install scipy
RUN apt-get install -y clang-tools-6.0
RUN cp /usr/lib/llvm-6.0/lib/libclang.so.1 /usr/lib/x86_64-linux-gnu 
RUN ln -s /usr/lib/x86_64-linux-gnu/libclang.so.1 /usr/lib/x86_64-linux-gnu/libclang.so 
RUN mkdir -p /perfbugdetection 
ADD . /perfbugdetection
RUN mv /perfbugdetection/clang /usr/lib/python3/dist-packages
