Prerequisites:
*  clang <= version 6
*  cindex - python interface for clang. Setting up usage of the Python bindings is very easy:``[Find further instructions here](https://eli.thegreenplace.net/2011/07/03/parsing-c-in-python-with-clang)``
*  python >= 3.5
*  c++ >= 11

Our experiments were performed on Ubuntu 16.04 Server.


1. To test push_back with reserve - *C++*
```
cd reserve
python3 push_back_reserve.py ../tests/loop.cpp <output_file.cpp> >
```


2. To test for loop to map - *Python*
```
cd for_to_map
python3 testast.py ../tests/python_for.py <output_file.py>
```


3. To test function pointer to functor -* C++*
```
cd ptr_to_functor
python3 function_pointer.py ../tests/selsort.cpp <binary>
python3 fptr_to_function.py <binary> ../tests/selsort.cpp <output_file.cpp>
```
 
Tool can be used by building Dockerfile in repository root by command
``` 
docker build -t pbd/test:v1 .
docker run -it pdb/test /bin/bash
cd perfbugdetection
```

