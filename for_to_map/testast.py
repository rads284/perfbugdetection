import ast
from pprint import pprint
import sys 

if len(sys.argv)==3:
    ip = sys.argv[1]
    op = sys.argv[2]

source  = open(ip,"r")
# lines = source.readlines()
func_dict = {}

def main():
    
    tree = ast.parse(source.read())
    res={}
    global func_dict
    for node in ast.walk(tree):
        # if isinstance(node, ast.FunctionDef):
        #     func_dict[node.name] = []
        #     for stmt in node.body:
        #         if isinstance(stmt.value, ast.Call):
        #             if isinstance(stmt.value.func, ast.Attribute):
        #                 if(stmt.value.func.attr == "append"):
        #                     # print(node.name, stmt.value.func.value.id)
        #                     func_dict[node.name].append(stmt.value.func.value.id)

        if isinstance(node, ast.For):
            # print(node,node.lineno)
            newA = Analyzer(node)
            newA.analyse()
            if(newA.consider):
                res[node.lineno] = newA
                print(newA.iterName, newA.titerName, newA.funcName, node.lineno)
    
    source.close()

    # Replacement 
    f = open(ip,"r")
    lines = f.readlines()
    pop_by = 0

    for key in res:
        for i,l in enumerate(lines):
            if i == key-1:
                lines.pop(i-pop_by)
                lines.pop(i-pop_by)
                pop_by += 2
                replacement = "{0} = map({1},{2})\n".format(res[key].titerName,res[key].funcName,res[key].iterName)

                lines.insert(i,replacement)
                pop_by -= 1

    new_file = open(op,"w")
    new_file.writelines(lines)




class Analyzer(ast.NodeVisitor):
    def __init__(self, node):
        self.targetName = (node.target.id)
        self.iterName = ""
        self.funcName = ""
        self.args = []
        self.titerName = ""
        self.consider = False
        argVals = []
        if isinstance(node.iter, ast.Name):
            self.iterName = node.iter.id
        if(len(node.body) == 1):
            self.tree = node.body[0]
        else:
            self.tree = None

    
    def analyse(self):
        global func_dict
        if(self.tree):
            for node in ast.walk(self.tree):
                if isinstance(node, ast.Call):
                    if isinstance(node.func, ast.Attribute):
                        if(node.func.attr == "append"):
                            self.consider = True
                            self.titerName = node.func.value.id
                            for it in node.args:
                                try:
                                    self.funcName = it.func.id
                                    for arg in it.args:
                                        self.args.append(arg.id)
                                except:
                                    pass
                        continue
                    # if isinstance(node.func, ast.Name):
                        # print(node.func.id,"hi")
                        # for arg in node.args:
                            # if isinstance(arg, ast.Name):
                            #     if self.targetName == arg.id:
                            #         self.consider = True
                            #         self.funcName = node.func.id
                            #         if self.funcName in func_dict:
                            #             self.titerName = func_dict[self.funcName][0]
                            # else:
                            #     continue

    



if __name__ == "__main__":
    main()
