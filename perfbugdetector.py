import argparse
import os
import sys
import random 
import string 
import time
from scipy.stats.mstats import gmean

def usage():
	print("Usage:\n python3 perfbugdetector.py -b [functor|reserve|map] " + \
		  "-i <inputfile> -o <outputfile>")

def functor(input_file,output_file):
	binary = ''.join(random.choices(string.ascii_uppercase , k = 5))
	result1 = os.system("python3 ptr_to_functor/function_pointer.py {0} {1} > funca".format(input_file,binary))
	result2 = os.system("python3 ptr_to_functor/fptr_to_functor.py {0} {1} {2} > funcb\
						".format(binary,input_file,output_file))

	os.system("rm {0}".format(binary))
	os.system("rm funca")
	os.system("rm funcb")

def reserve(input_file,output_file):
	result = os.system("python3 reserve/push_back_reserve.py {0} {1} > reservea"\
						    .format(input_file,output_file))
	os.system("rm reservea")
def map(input_file,output_file):
	result = os.system("python3 for_to_map/testast.py {0} {1} > mapa"\
							.format(input_file,output_file))
	os.system("rm mapa")

def performanceCPP(input_file,output_file):
	inputtimes = []
	outputtimes = []

	os.system("g++ {0} -std=c++14".format(input_file))
	for i in range(10):
		start = time.time()
		os.system("./a.out")
		end = time.time()
		inputtimes.append(end-start)

	os.system("g++ {0} -std=c++14".format(output_file))
	for i in range(10):
		start = time.time()
		os.system("./a.out")
		end = time.time()
		outputtimes.append(end-start)
	
	gmean1 = gmean(inputtimes)
	gmean2 = gmean(outputtimes)

	perc = ((gmean1 - gmean2)/gmean1)*100
	return perc
	
def performancePy(input_file,output_file):
	inputtimes = []
	outputtimes = []

	for i in range(10):
		start = time.time()
		os.system("python3 {0}".format(input_file))
		end = time.time()
		inputtimes.append(end-start)

	for i in range(10):
		start = time.time()
		os.system("python3 {0}".format(output_file))
		end = time.time()
		outputtimes.append(end-start)
	
	gmean1 = gmean(inputtimes)
	gmean2 = gmean(outputtimes)

	perc = ((gmean1 - gmean2)/gmean1)*100
	return perc


def checkVersions():
	return True

if __name__ == "__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-b", "--bug",
						help="Scans the code for performance imporvements. The options can be " + \
						"functor , reserve , map")
	parser.add_argument("-i","--input",
						help="Input file to the performance bug detector - Can be a C++ or Python3 File")
	parser.add_argument("-o","--output",
						help="Output file of the performance bug detector. Can be comma separated list\
						of files when all is passed as a bug")

	args = parser.parse_args()

	if not args.input or not args.output or not args.bug:
		usage()
		sys.exit(1)

	if not checkVersions():
		print("Incorrect Versions")
		sys.exit(1)

	input_file = args.input
	output_file = args.output
	bug = args.bug

	if bug == "all":
		files = output_file.split(",")
		functor(input_file,files[0])
		reserve(input_file,files[1])
		percImprove = performanceCPP(input_file,files[0])
		print("Suggestions : Please Read {0} File for the changed code with optimizations"\
			  .format(files[0]))
		print("Percentage Improvements : {0}".format(percImprove))

		percImprove = performanceCPP(input_file,files[1])
		print("Suggestions : Please Read {0} File for the changed code with optimizations"\
			  .format(files[1]))
		print("Percentage Improvements : {0}".format(percImprove))

	elif bug == "functor":
		functor(input_file,output_file)
		percImprove = performanceCPP(input_file,output_file)
		print("Suggestions : Please Read {0} File for the changed code with optimizations"\
			  .format(output_file))
		print("Percentage Improvements : {0}".format(percImprove))

	elif bug == "reserve":
		reserve(input_file,output_file)
		percImprove = performanceCPP(input_file,output_file)
		print("Suggestions : Please Read {0} File for the changed code with optimizations"\
			  .format(output_file))
		print("Percentage Improvements : {0}".format(percImprove))

	elif bug == "map":
		map(input_file,output_file)
		percImprove = performancePy(input_file,output_file)
		print("Suggestions : Please Read {0} File for the changed code with optimizations"\
			  .format(output_file))
		print("Percentage Improvements : {0}".format(percImprove))

	else:
		usage()
		sys.exit(1)
