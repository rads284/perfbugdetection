import re
import os 
import sys
import pickle
import json

p = r'[a-zA-Z]+[\s]*\(\*\)[\s]*\([a-zA-Z,\s]*\)'
fptr_regex = re.compile(p)
function_dict = dict()
return_dict = dict()

call_expr_list = [{
    "name":"foo",
    "parameters":[{"name":"bar","type":"void(*)()"}]
  },{
    "name":"bar",
    "parameters":[]
  }
  ]

call_decl_list = [{
    "name":"foo",
    "start":1,
    "end":2,
    "parameters":[{"name":"func1","type":"void(*)()"}]
  },{
    "name":"bar",
    "start":10,
    "end":15,
    "parameters":[{"name":"a", "type":"int"},{"name":"b","type":"double"}]
  }
  ]

#ip = {"CursorKind.FUNCTION_DECL":[{"name":"foo2","start":6,"end":6,"parameters":[{"name":"x","type":"int"}]},{"name":"foo","start":18,"end":18,"parameters":[{"name":"i","type":"int"}]},{"name":"bar","start":19,"end":21,"parameters":[{"name":"temp","type":"void (*)(int)"},{"name":"var","type":"int"}]},{"name":"main","start":22,"end":29,"parameters":[]}],"CursorKind.CALL_EXPR":[{"name":"temp","start":20,"end":20,"parameters":[{"name":"temp","type":"void (*)(int)"},{"name":"var","type":"int"}]},{"name":"foo","start":24,"end":24,"parameters":[{"name":"foo","type":"void (int)"}]},{"name":"foo2","start":25,"end":25,"parameters":[{"name":"foo2","type":"void (int)"}]},{"name":"bar","start":26,"end":26,"parameters":[{"name":"bar","type":"void (void (*)(int), int)"},{"name":"foo","type":"void (int)"},{"name":"a","type":"int"}]},{"name":"bar","start":27,"end":27,"parameters":[{"name":"bar","type":"void (void (*)(int), int)"},{"name":"foo2","type":"void (int)"},{"name":"a","type":"int"}]}]}
ip_file = sys.argv[1]
test_file = sys.argv[2]
op_file = sys.argv[3]
objects = []
with (open(ip_file, "rb")) as openfile:
     openfile.seek(0)
     while True:
        try:
            objects.append(pickle.load(openfile))
        except EOFError:
            break
ip = objects[0]


print('radhika legacy', json.dumps(ip))
call_decl_list = ip["CursorKind.FUNCTION_DECL"]
call_expr_list = ip["CursorKind.CALL_EXPR"]

# print(call_expr_list)
# print(  "\n\n\n\n")
for function in call_decl_list:
    temp = dict()
    temp["start"] = function["start"]
    temp["end"] = function["end"]
    temp["parameters"] = []
    temp["args"] = []
    for param in function["parameters"]:
        temp["args"].append(param)
        param_str = "{} {}".format(param["type"], param["name"])
        temp["parameters"].append(param_str)
    function_dict[function["name"]] = temp

# print(function_dict)
temp =  set()
for call_exp in call_expr_list:
    # print(call_exp["name"])
    self_name = call_exp["name"]
    call_exp["parameters"] = call_exp["parameters"][1:]
    if self_name in function_dict:
        call_decl = function_dict[self_name]
        index = 0
        # print(call_decl["args"])
        for arg in call_decl["args"]:
            print(self_name, 'arg',arg)
            if(fptr_regex.match(arg["type"])):
                print('kaka',arg["type"], call_exp["parameters"][index])
                fname = call_exp["parameters"][index]['name']
                if fname in return_dict:
                    return_dict[fname]["calls"].append({"start":call_exp["start"],"end":call_exp["end"]})
                else:
                    return_dict[fname] = dict()
                    # return_dict[fname]["call_start"] = call_exp["start"]
                    # return_dict[fname]["call_end"] = call_exp["end"]
                    return_dict[fname]["calls"] = [{"start":call_exp["start"],"end":call_exp["end"]}]
                    return_dict[fname]["parameters"] = function_dict[fname]["parameters"]
                    return_dict[fname]["def_start"] = function_dict[fname]["start"]
                    return_dict[fname]["def_end"] = function_dict[fname]["end"]
                    return_dict[fname]["caller_def_start"] = function_dict[self_name]["start"]
                    temp.add(return_dict[fname]["caller_def_start"])
                    return_dict[fname]["caller_def_end"] = function_dict[self_name]["end"]
            index += 1 
# print(function_dict)
print(return_dict)

# linestart = 16
# lineend = 21
# name = 'foo'
return_type = 'int'

# callstart = 30
# callend = 31
# function_code = ""

# calleestart = 22
# calleeend = 25

# parameters = ["name","var1","type","int"]
functor_body = []
for func in return_dict:
    function_code = ""
    def_start = return_dict[func]["def_start"]
    def_end = return_dict[func]["def_end"]
    params = ', '.join(return_dict[func]['parameters'])
    with open(test_file) as fp:
        for i, line in enumerate(fp):
            if i >= def_start -1 and i < def_end :
                function_code += line

    function = re.compile(r'\{(((.*)\n)*)\}')
    print('ssssssssssss',function_code,'\n\n')
    function_code = function_code[function_code.find('{')+1:function_code.rfind('}')-1]
    body = [func, return_type, params, function_code]
    functor = '''\n\nclass functor_{0} {{ 
        public:
        __attribute__((always_inline)) {1} operator()({2}){{
            {3}
        }}
    }};\n\n'''.format(*body)

    print(functor,'\n\n')

    return_dict[func]["body"] = functor
    functor_body.append([def_end,return_dict[func]])

lines = open(test_file,'r').readlines()
#print(len(lines))

fptr = re.compile(r'([a-zA-Z]+[\s]*\(\*([a-zA-Z]*)\)[\s]*\([a-zA-Z,\s]*\))')
for def_start in temp:
    print('yolox', lines[def_start-1])
    print('yolox', fptr.findall(lines[def_start-1]))
    arg = fptr.findall(lines[def_start-1])[0][1]
    lines[def_start-1] = re.sub(fptr,'auto '+arg,lines[def_start-1])
    print('yolo ', lines[def_start-1])

offset = 0
for line_start in  sorted(functor_body, key=lambda x: x[0]):
    lines.insert(line_start[0]+offset,line_start[1]['body'])
    offset+=1
'''    for param in line_start[1]['calls']:
        print(lines[param['start']+offset+1])
'''
#print(len(lines))

print(return_dict,'\n\n\n')
for fun in return_dict:
    for callee in return_dict[fun]["calls"]:
        line_no = callee['start']+offset -1 
        print(callee,line_no,fun)
        lines[line_no] = lines[line_no].replace(fun,"functor_"+fun+"()")

        print(lines[line_no])


'''function_call = ""
with open('extern2.cpp', 'r') as f:
    for i, line in enumerate(f):
        if i == def_start - 2:
            line = line + "\n\n//Functor Code\n"+functor+"//End of Functor Code\n\n"
        if i == def_end - 1:
            line = line + "\n\n"
        if i == return_dict[func]["call_start"] - 1:
            function_call = function_call + line
            line = "/**" + line
        # if i == calleestart - 1:
        #     line = "inline " + line
        if i > return_dict[func]["call_start"] - 1 and i < return_dict[func]["call_end"]:
            function_call = function_call + line
        if i == return_dict[func]["call_end"]-1:
            #print(function_call)  
            function_call = re.sub('[\s]',"",function_call)
            callee = function_call[:function_call.find('(')] + "("
            params_list = function_call[function_call.find('(')+1:function_call.rfind(')')].split(",")
            for i,param in enumerate(params_list):
                print('paramprint',i,param,func)
                if param == func:
                    params_list[i] = "functor_" + func + "()()"
            params_list = callee + ",".join(params_list) + ")"
            print("params_list:",params_list)
            line = line + "*/\n\n //New Code Here \n{0} ; \n\n ".format(params_list)
            #print(line)
        lines.append(line)
'''
with open(op_file,'w') as f:
    for line in lines:
        f.write(line)


#    os.system("time g++ temp.cpp")
#os.system("time g++ extern2.cpp")
    



