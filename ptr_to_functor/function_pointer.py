#!/usr/bin/env python3
import sys
import clang.cindex
import pprint
pp = pprint.PrettyPrinter(indent=2)
INDENT = 4
K = clang.cindex.CursorKind

def is_std_ns(node):
    return node.kind == K.NAMESPACE and node.spelling == 'std'

out = {str(K.FUNCTION_DECL):[], str(K.CALL_EXPR):[]}

fd = -1 # FUCNTION_DECL
ce = -1 # CALL_EXPR
parm = -1 # PARAM_DECL
dre = -1 # DECL_REF_EXPR
prev = ''
def vit(node: clang.cindex.Cursor, indent: int, saw, prev):
    global fd,ce,parm,dre
    pre = ' ' * indent
    k = node.kind  # type: clang.cindex.CursorKind
    kind = str(node.kind)
    if k == K.CXX_METHOD:
        k = K.FUNCTION_DECL
        kind = str(k)
    # skip printting UNEXPOSED_*
    if (not k.is_unexposed() and k != K.DECL_REF_EXPR ) or (prev == K.CALL_EXPR and k == K.DECL_REF_EXPR and not k.is_unexposed())  :
#    if not k.is_unexposed():
        print(prev,'abc',pre, end='')
        print('def',k.name, end=' ')
        if node.lexical_parent and node.semantic_parent:
            print('node',node.lexical_parent.kind,'a',node.semantic_parent.kind)
        if node.spelling:
            print('s:', node.spelling, end=' ')
            if k == K.FUNCTION_DECL:
                parm = -1
                fd+=1
                out[kind].append({})
                out[kind][fd]['name'] = node.spelling
                out[kind][fd]['start'] = node.extent.start.line
                out[kind][fd]['end'] = node.extent.end.line
                out[kind][fd]['parameters'] = []
            elif k == K.CALL_EXPR:
                ce+=1
                dre = -1
                out[kind].append({})
                out[kind][ce]['name'] = node.spelling
                out[kind][ce]['start'] = node.extent.start.line
                out[kind][ce]['end'] = node.extent.end.line
                out[kind][ce]['parameters'] = []
            elif k == K.PARM_DECL:
                parm+=1
                out[str(K.FUNCTION_DECL)][fd]['parameters'].append({})
                out[str(K.FUNCTION_DECL)][fd]['parameters'][parm]['name']= node.spelling
                out[str(K.FUNCTION_DECL)][fd]['parameters'][parm]['type'] = node.type.spelling
            elif k == K.DECL_REF_EXPR:
                dre+=1
                out[str(K.CALL_EXPR)][ce]['parameters'].append({})
                out[str(K.CALL_EXPR)][ce]['parameters'][dre]['name']= node.spelling
                out[str(K.CALL_EXPR)][ce]['parameters'][dre]['type'] = node.type.spelling
            if node.type.spelling:
                print('t:', node.type.spelling, end=' ')
            print ('extent: ', node.extent.start.line, node.extent.start.column, node.extent.end.line, node.extent.end.column, sep=',')
            # FIXME: print opcode or literal
        print()
    saw.add(node.hash)
    #if node.referenced is not None and node.referenced.hash not in saw:
    skip = len([c for c in node.get_children()
                if is_std_ns(c)])
    for c in node.get_children():
        if not skip:
            if not k.is_unexposed():
                prev = k
            vit(c, indent + INDENT, saw, prev)
        if indent == 0 and is_std_ns(c):
            skip -= 1
    saw.remove(node.hash)

def main():
    index = clang.cindex.Index.create()
    tu = index.parse(sys.argv[1])
    vit(tu.cursor, 0, set(),'')

main()
import pickle
pp.pprint(out)
with open(sys.argv[2], 'wb') as fp:
    pickle.dump(out, fp)
