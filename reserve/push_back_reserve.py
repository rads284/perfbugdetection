
#!/usr/bin/env python3
import sys
import clang.cindex
import pprint
import re
pp = pprint.PrettyPrinter(indent=2)
INDENT = 4
#clang.cindex.Config.set_library_path('/usr/local/Cellar/llvm@6/6.0.1_2/lib')
K = clang.cindex.CursorKind

#function to check if cursor belongs to standard namespace or global namespace
def is_std_ns(node):
    return node.kind == K.NAMESPACE and node.spelling == 'std'

#output list containing parameters (for loop extents, name of vector, for_flag, if_flag) as list of lists with an entry for every push_back call
out = []

def for_parse(node: clang.cindex.Cursor):
    for c in node.get_children():
        if c.kind == K.BINARY_OPERATOR:
            i = 0
            for d in c.get_children():
                if i==1 and d.kind == K.INTEGER_LITERAL:
                   try:
                        x = d.spelling
                        return x
                   except:
                        pass
                elif i==1:
                    for e in d.get_children():
                        if e.kind == K.DECL_REF_EXPR:
                            x = e.spelling
                            return x
                i+=1

'''vit is a function that recursively traverses ast tree
saw is a list that keeps track of previously visited nodes by  hash values
prev is a variable that holds the previous node
for_flg is a flag that gets set as True when current node is a for statement
pb_flg is a flag that gets set as True when current node is a call expression of type push_back
if_flg is a flag that gets set as True when current node is an if statement
for_ext contains the start line and end line of for statement'''
def vit(node: clang.cindex.Cursor, indent: int, saw, prev,for_flg,pb_flg,if_flg,for_ext):
    pre = ' ' * indent
    k = node.kind  # type: clang.cindex.CursorKind
    kind = str(node.kind)
    if not k.is_unexposed():
        #check if node is of type for statement
        if k == K.FOR_STMT:
            for_out = for_parse(node)
            for_ext = [node.extent.start.line, node.extent.end.line]
            for_flg = for_out
        #check if node is of type for statement
        elif k == K.CALL_EXPR and node.spelling == 'push_back':
            pb_flg = True
        elif k == K.IF_STMT:
            if_flg = True
        elif pb_flg and k == K.DECL_REF_EXPR :
            out.append([for_ext,node.spelling,for_flg,if_flg])
    saw.add(node.hash)
    skip = len([c for c in node.get_children()
                if is_std_ns(c)])
    for c in node.get_children():
        if not skip:
            if not k.is_unexposed():
                prev = k
            vit(c, indent + INDENT, saw, prev, for_flg,pb_flg,if_flg,for_ext)
        if indent == 0 and is_std_ns(c):
            skip -= 1
    saw.remove(node.hash)

def main():
    index = clang.cindex.Index.create()
    tu = index.parse(sys.argv[1])
    vit(tu.cursor, 0, set(),'',False,False,False,None)

main()
import pickle
pp.pprint(out)

newout = []
f = open(sys.argv[1],'r')
lines = f.readlines()
pattern = "for\([^;]*;[a-zA-Z]+[<>!=]+([0-9]+);[^;]*\)"
p = re.compile(pattern)
added = 0
print(out)
for loop in out:
        if loop[0] and not loop[3]:
                start = int(loop[0][0])
                if loop[2]=='':
                    n = p.findall(lines[start-1+added])[0]
                else:
                    n = loop[2]
                replacement = "{0}.reserve({1});\n".format(loop[1],n)
                lines.insert(loop[0][0]-1+added,replacement)  # inserts "somedata" above the current line
                added = added + 1

with open(sys.argv[2],'w+') as f:
    f.truncate(0)         # truncates the file
    f.seek(0)             # moves the pointer to the start of the file
    f.writelines(lines)   # write the new data to the file
