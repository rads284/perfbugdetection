#include <algorithm> // for std::swap, use <utility> instead if C++11
#include <iostream>
#include <ctime> 
void selectionSort(int *array, int size, bool (*comparisonFcn)(int, int))
{
    // Step through each element of the array
    for (int startIndex = 0 ; startIndex < size - 1; ++startIndex)
    {
        // bestIndex is the index of the smallest/largest element we've encountered so far.
        int bestIndex = startIndex ;
 
        // Look for smallest/largest element remaining in the array (starting at startIndex+1)
        for (int currentIndex = startIndex + 1 ; currentIndex < size; ++currentIndex)
        {
            // If the current element is smaller/larger than our previously found smallest
            if (comparisonFcn(array[bestIndex], array[currentIndex])) // COMPARISON DONE HERE
                // This is the new smallest/largest number for this iteration
                bestIndex = currentIndex;
        }
 
        // Swap our start element with our smallest/largest element
        std::swap(array[startIndex], array[bestIndex]);
    }
}
 
// Here is a comparison function that sorts in ascending order
// (Note: it's exactly the same as the previous ascending() function)
bool ascending(int x, int y)
{
    return x > y; // swap if the first element is greater than the second
}
 
// Here is a comparison function that sorts in descending order
bool descending(int x, int y)
{
    return x < y; // swap if the second element is greater than the first
}
 
// This function prints out the values in the array
void printArray(int *array, int size)
{
    for (int index = 0 ; index < size; ++index)
        std::cout << array[index] << ' ';
    std::cout << '\n';
}
 
int main()
{
    clock_t s,t;
    int k =3200;
    int array[3200];
    for (int j=0; j<5;j++){
        for(int i=0;i<k;++i){
            array[i] = k-i;
        }
        t = clock();
        selectionSort(array, k, ascending);
        s += clock() -t;
    }
    // std::cout<<s<<std::endl; 
    return 0;
}
